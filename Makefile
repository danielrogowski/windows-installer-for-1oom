.PHONY: clean
CONFIGURE_OPTS=--disable-hwsdl1 --disable-hwx11 --disable-hwalleg4
INNO_COMPILER=/c/Program Files (x86)/Inno Setup 6/ISCC.exe

installer: SDL2.dll SDL2_mixer.dll build 1oom-installer.exe

build: configure 1oom.exe

configure: 1oom-build 1oom/configure 1oom-build/Makefile

clean:
	@rm -rf 1oom/configure 1oom-build 1oom.exe 1oom-installer.exe || true

1oom-build:
	mkdir 1oom-build

1oom/configure:
	cd 1oom; autoreconf -fi

1oom-build/Makefile:
	cd 1oom-build; ../1oom/configure ${CONFIGURE_OPTS}

1oom.exe:
	make -C 1oom-build
	cp 1oom-build/src/1oom_classic_sdl2 1oom.exe

1oom-installer.exe:
	"${INNO_COMPILER}" -O. -F1oom-installer installer_script.iss

SDL2.dll:
	@echo "SDL2.dll is missing, please download it and copy it here."
	false

SDL2_mixer.dll:
	@echo "SDL2_mixer.dll is missing, please download it and copy it here."
	false
